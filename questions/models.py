from django.db import models

from components.models import Option


class Question(models.Model):
    LAST = -1

    text = models.CharField(max_length=200)
    position = models.SmallIntegerField(default=LAST)

    def __str__(self):
        return self.text


class Choice(models.Model):
    question = models.ForeignKey(
        Question,
        models.CASCADE,
        related_name='choices'
    )
    followups = models.ManyToManyField(
        Question,
        blank=True,
        related_name='leading_choices'
    )
    text = models.CharField(max_length=200)
    options = models.ManyToManyField(Option, blank=True)

    def __str__(self):
        return '%s - %s' % (self.question, self.text)
