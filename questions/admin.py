from django.contrib import admin

from utils.admin import TemperateTabularInline
from .models import Question, Choice


class ChoiceInline(TemperateTabularInline):
    model = Choice
    fields = ('text', 'options', 'followups',)


class QuestionAdmin(admin.ModelAdmin):
    inlines = (ChoiceInline,)
    list_display = ('text', 'position',)


admin.site.register(Question, QuestionAdmin)
