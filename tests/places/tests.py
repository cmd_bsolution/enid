import random
from unittest.mock import patch
from unittest import skip

from django.test import TestCase
from django.urls import reverse

from rest_framework.test import APITestCase

from components.models import Option
from users.models import User

from places.models import Place
from places.utils import SecurityRatingCalculator

from ..factories import ComponentFactory, OptionFactory, PlaceFactory, FACTORY_PASSWORD


class PlaceModelTest(TestCase):

    @classmethod
    def setUpTestData(cls):
        detection_component = ComponentFactory(parent=None)
        detection_component_option = OptionFactory(component=detection_component, score=1)

        heavier_detection_component = ComponentFactory(parent=None, weights={'detection': 3})
        heavier_detection_component_option = OptionFactory(component=heavier_detection_component, score=5)

        delay_component = ComponentFactory(parent=None, weights={'delay': 1})
        delay_component_option = OptionFactory(component=delay_component, score=1)

        child_component = ComponentFactory(parent=detection_component_option)
        child_component_option = OptionFactory(component=child_component, score=3)

        detection_options = (
            detection_component_option,
            heavier_detection_component_option,
            child_component_option,
        )
        mixed_factor_options = detection_options + (delay_component_option,)

        cls.place1 = PlaceFactory(options=detection_options)
        cls.place2 = PlaceFactory(options=mixed_factor_options)

    # FIXME: moved to places.utils.SecurityRatingCalculator
    @skip
    def test_get_options_mean(self):
        EXPECTED_MEAN_SCORE = 4.25
        mean_score = self.place1.get_options_mean('detection')

        self.assertEqual(mean_score, EXPECTED_MEAN_SCORE)

    # FIXME: moved to places.utils.SecurityRatingCalculator
    @skip
    def test_get_options_mean_calculates_a_single_factor(self):
        """
        Test whether a component with weight on a different factor
        than the one passed as a parameter affects the return value of
        the `get_options_mean` method.
        """
        mean_score1 = self.place1.get_options_mean('detection')
        mean_score2 = self.place2.get_options_mean('detection')

        self.assertEqual(mean_score1, mean_score2)

    # FIXME: moved to places.utils.SecurityRatingCalculator
    @skip
    @patch('places.utils.SecurityRatingCalculator.get_options_mean')
    def test_security_rating(self, mock_get_options_mean):

        # Arbitrary values for every factor for which
        # `self.get_options_mean` will be called.
        BEHAVIOR_MEAN = 1.0
        DELAY_MEAN = 2.0
        DETECTION_MEAN = 3.0
        DETERRENCE_MEAN = 4.0
        PROPERTY_TYPE_MEAN = 5.0
        RESPONSE_MEAN = 1.0
        CRIME_DEMOGRAPHICS_MEAN = 2.0
        CONDITIONS_MEAN = 3.0
        LOCATION_MEAN = 4.0
        DAMAGE_MEAN = 25.0
        SEVERITY_MEAN = 5.0

        EXPECTED_RATING = 240.0

        mock_get_options_mean.side_effect = [
            BEHAVIOR_MEAN,
            DELAY_MEAN,
            DETECTION_MEAN,
            DETERRENCE_MEAN,
            PROPERTY_TYPE_MEAN,
            RESPONSE_MEAN,
            CRIME_DEMOGRAPHICS_MEAN,
            CONDITIONS_MEAN,
            LOCATION_MEAN,
            DAMAGE_MEAN,
            SEVERITY_MEAN,
        ]

        self.assertEqual(Place().security_rating, EXPECTED_RATING)


class TestPlaceViewset(APITestCase):
    @classmethod
    def setUpTestData(cls):
        cls.user = User.objects.create_superuser(
            email='admin@home.com',
            password=FACTORY_PASSWORD,
        )
        factors = (
            'behavior',
            'delay',
            'detection',
            'deterrence',
            'property_type',
            'response',
            'crime_demographics',
            'conditions',
            'location',
            'severity',
            'damage',
        )

        for factor in factors:
            component = ComponentFactory.create(weights={factor: 1.2}, parent=None)
            OptionFactory.create(component=component)

        cls.options_urls = [
            reverse('option-detail', kwargs={'pk': o.id}) for o in Option.objects.all()]

    def setUp(self):
        self.client.login(email=self.user.email, password=FACTORY_PASSWORD)

    def test_create_place(self):
        response = self.client.post(
            reverse('place-list'),
            data={
                'address': 'here',
                'name': 'this',
                'owner': User.objects.first().id,
                'options': self.options_urls,
            }
        )
        self.assertEqual(response.status_code, 201)

    def test_create_place_missed_factor(self):
        response = self.client.post(
            reverse('place-list'),
            data={
                'address': 'here',
                'name': 'this',
                'owner': User.objects.first().id,
                'options': self.options_urls[:-1],
            }
        )
        self.assertEqual(response.status_code, 400)


class SecurityRatingCalculatorTest(TestCase):
    def test_get_options_mean(self):
        EXPECTED_MEAN_SCORE = 4.25
        detection_component = ComponentFactory(parent=None)
        detection_component_option = OptionFactory(component=detection_component, score=1)
        heavier_detection_component = ComponentFactory(parent=None, weights={'detection': 3})
        heavier_option = OptionFactory(component=heavier_detection_component, score=5)
        child_component = ComponentFactory(parent=detection_component_option)
        child_option = OptionFactory(component=child_component, score=3)
        options = Option.objects.filter(
            id__in=[detection_component_option.id, heavier_option.id, child_option.id])
        calculator = SecurityRatingCalculator(options)
        mean_score = calculator.get_options_mean('detection')

        self.assertEqual(mean_score, EXPECTED_MEAN_SCORE)

    @patch('places.utils.SecurityRatingCalculator.get_options_mean')
    def test_security_rating(self, mock_get_options_mean):

        # Arbitrary values for every factor for which
        # `self.get_options_mean` will be called.
        BEHAVIOR_MEAN = 1.0
        DELAY_MEAN = 2.0
        DETECTION_MEAN = 3.0
        DETERRENCE_MEAN = 4.0
        PROPERTY_TYPE_MEAN = 5.0
        RESPONSE_MEAN = 1.0
        CRIME_DEMOGRAPHICS_MEAN = 2.0
        CONDITIONS_MEAN = 3.0
        LOCATION_MEAN = 4.0
        DAMAGE_MEAN = 25.0
        SEVERITY_MEAN = 5.0

        EXPECTED_RATING = 240.0

        mock_get_options_mean.side_effect = [
            BEHAVIOR_MEAN,
            DELAY_MEAN,
            DETECTION_MEAN,
            DETERRENCE_MEAN,
            PROPERTY_TYPE_MEAN,
            RESPONSE_MEAN,
            CRIME_DEMOGRAPHICS_MEAN,
            CONDITIONS_MEAN,
            LOCATION_MEAN,
            DAMAGE_MEAN,
            SEVERITY_MEAN,
        ]

        self.assertEqual(SecurityRatingCalculator({}).calculate(), EXPECTED_RATING)


class CalculateSecuirtyTestAPITest(APITestCase):
    @classmethod
    def setUpTestData(cls):
        cls.factors = (
            'behavior',
            'delay',
            'detection',
            'deterrence',
            'property_type',
            'response',
            'crime_demographics',
            'conditions',
            'location',
            'severity',
            'damage',
        )
        options = []
        for factor in cls.factors:
            component = ComponentFactory.create(weights={factor: 1.2}, parent=None)
            options.append(OptionFactory.create(component=component))

        cls.options_urls = [
            reverse('option-detail', kwargs={'pk': o.id})
            for o in options]

    def test_calculate_security_rating_on_the_fly(self):
        response = self.client.post(
            reverse('place-calculate-security-rating-list'),
            data={'options': self.options_urls}
        )
        self.assertEqual(response.status_code, 200)

    def test_calculate_security_rating_missed_data(self):
        response = self.client.post(
            reverse('place-calculate-security-rating-list'),
            data={'options': self.options_urls[:-1]}
        )
        self.assertEqual(response.status_code, 400)
