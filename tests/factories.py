import factory
from factory import fuzzy


FACTORY_PASSWORD = 'factory_password'


class UserFactory(factory.django.DjangoModelFactory):

    class Meta:
        model = 'users.User'

    name = factory.Faker('name')
    email = factory.Faker('email')
    password = FACTORY_PASSWORD
    email_verified = True

    @classmethod
    def _create(cls, model_class, *args, **kwargs):
        """
        Create a User instance using the `create_user` manager method.
        """
        manager = cls._get_manager(model_class)
        return manager.create_user(*args, **kwargs)


class UnactiveUserFactory(UserFactory):
    is_active = False


class UnverifiedUserFactory(UserFactory):
    email_verified = False
    is_active = False


class ComponentFactory(factory.django.DjangoModelFactory):

    class Meta:
        model = 'components.Component'

    name = factory.Sequence(lambda n: 'Component%d' % n)
    weights = {
        'detection': 1
    }
    parent = factory.SubFactory('tests.factories.OptionFactory')
    is_mitigation_measure = True


class OptionFactory(factory.django.DjangoModelFactory):

    class Meta:
        model = 'components.Option'

    name = factory.Sequence(lambda n: 'Option%d' % n)
    component = factory.SubFactory(ComponentFactory)
    score = fuzzy.FuzzyInteger(1, 5)


class PlaceFactory(factory.django.DjangoModelFactory):

    class Meta:
        model = 'places.Place'

    address = factory.Faker('address')
    name = fuzzy.FuzzyText()
    owner = factory.SubFactory(UserFactory)

    @factory.post_generation
    def options(self, create, extracted, **kwargs):
        if not create:
            return
        elif extracted:
            for option in extracted:
                self.options.add(option)
