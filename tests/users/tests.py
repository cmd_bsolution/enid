import re
from rest_framework.test import APITestCase
from django.urls import reverse
from django.core import mail
from django.conf import settings

from oauth2_provider.models import Application
from users.models import User
from ..factories import (
    UserFactory, UnactiveUserFactory, UnverifiedUserFactory, FACTORY_PASSWORD,
)

USER_ACTIVATES_ACCOUNT = settings.DJOSER['SEND_ACTIVATION_EMAIL']


class UserTestCases(APITestCase):

    def setUp(self):
        self.valid_redirect_uri = 'http://localhost:8000/callback/'
        # create application
        self.app = Application.objects.create(
            client_id='good_client_id123', client_type='public',
            skip_authorization=True, authorization_grant_type='client-credentials', name='test1'
        )
        # login with application
        response = self.client.post(
            '/api/auth/token/',
            data={
                'client_id': self.app.client_id,
                'client_secret': self.app.client_secret,
                'grant_type': 'client_credentials',
            }
        )
        self.assertEqual(response.status_code, 200, response.content)
        self.token = 'Bearer ' + response.json()['access_token']
        # Authenticate all tests
        self.client.credentials(HTTP_AUTHORIZATION=self.token)

        self.users_app_client_id = 'UsersApp'
        self.users_app_secret = 'SECRET'
        self.users_app = Application.objects.create(
            client_id=self.users_app_client_id, client_secret=self.users_app_secret, client_type='public',
            skip_authorization=True, authorization_grant_type='password'
        )

    def test_create_superuser(self):
        admin = User.objects.create_superuser(
            email='admin@home.com',
            password=FACTORY_PASSWORD,
        )
        self.assertEqual(admin.is_superuser, True)
        self.assertEqual(admin.is_staff, True)
        self.assertEqual(admin.check_password(FACTORY_PASSWORD), True)

        self.assertEqual(str(admin), 'admin@home.com')
        self.assertEqual(admin.get_short_name(), 'admin@home.com')
        admin.name = 'Admin Junior'
        admin.save()
        self.assertEqual(admin.get_short_name(), 'Admin Junior')
        self.assertEqual(admin.get_full_name(), 'Admin Junior')

    def test_user_registration_flow(self):
        # test system has no users
        self.assertEqual(User.objects.all().count(), 0)
        # test user signup
        response = self.client.post(
            reverse('auth:register'),
            data={
                'email': 'cal@krypton.com',
                'password': FACTORY_PASSWORD
            }
        )
        self.assertEqual(response.status_code, 201, response.content)
        # test user exists
        self.assertEqual(User.objects.all().count(), 1)

        if USER_ACTIVATES_ACCOUNT:
            # test user can't login
            response = self.client.post(
                reverse('auth:token'),
                data={
                    'client_id': self.users_app_client_id,
                    'client_secret': self.users_app_secret,
                    'grant_type': 'password',
                    'username': 'cal@krypton.com',
                    'password': FACTORY_PASSWORD
                }
            )
            self.assertEqual(response.status_code, 400, response.content)

        # Activate user manually because mail activation is already tested in another test
        user = User.objects.first()
        user.is_active = True
        user.save()

        # test user can login after activation
        response = self.client.post(
            reverse('auth:token'),
            data={
                'client_id': self.users_app_client_id,
                'client_secret': self.users_app_secret,
                'grant_type': 'password',
                'username': 'cal@krypton.com',
                'password': FACTORY_PASSWORD
            }
        )
        self.assertEqual(response.status_code, 200, response.content)

    def test_user_can_login_and_see_profile(self):
        user = UserFactory.create()
        response = self.client.post(
            reverse('auth:token'),
            data={
                'client_id': self.users_app_client_id,
                'client_secret': self.users_app_secret,
                'grant_type': 'password',
                'username': user.email,
                'password': FACTORY_PASSWORD
            }
        )
        self.assertEqual(response.status_code, 200)
        token = 'Bearer ' + response.json()['access_token']
        self.client.credentials(HTTP_AUTHORIZATION=token)
        # Now, user should be logged-in
        response = self.client.get(
            reverse('auth:user'),
        )
        self.assertEqual(response.status_code, 200)

    def test_loggedin_user_can_disable_account(self):
        user = UserFactory.create()
        response = self.client.post(
            reverse('auth:token'),
            data={
                'client_id': self.users_app_client_id,
                'client_secret': self.users_app_secret,
                'grant_type': 'password',
                'username': user.email,
                'password': FACTORY_PASSWORD
            }
        )
        self.assertEqual(response.status_code, 200)
        token = 'Bearer ' + response.json()['access_token']
        self.client.credentials(HTTP_AUTHORIZATION=token)

        # Now, user should be logged-in

        response = self.client.post(
            reverse('auth:deactivate_account')
        )
        self.assertEqual(response.status_code, 200)
        # call it again to make sure user isn't authenticated anymore
        response = self.client.post(
            reverse('auth:deactivate_account')
        )
        self.assertEqual(response.status_code, 401)

    def test_user_can_reactivate_account(self):
        user = UnactiveUserFactory()

        # test user can't login
        self.assertFalse(self.client.login(email=user.email, password=FACTORY_PASSWORD))

        # call login with activate=True
        response = self.client.post(
            reverse('auth:token'),
            data={
                'client_id': self.users_app_client_id,
                'client_secret': self.users_app_secret,
                'grant_type': 'password',
                'username': user.email,
                'password': FACTORY_PASSWORD,
                'activate': True
            }
        )
        self.assertEqual(response.status_code, 200)

    def test_unverified_users_cant_activate_their_accounts_illegally(self):
        """
        This is to test that unverified users can't login by setting
        activate=True
        """
        user = UnverifiedUserFactory()

        # call login with activate=True
        response = self.client.post(
            reverse('auth:token'),
            data={
                'client_id': self.users_app_client_id,
                'client_secret': self.users_app_secret,
                'grant_type': 'password',
                'username': user.email,
                'password': FACTORY_PASSWORD,
                'activate': True
            }
        )
        self.assertEqual(response.status_code, 400)

    def test_unverified_users_and_wrong_credentials_and_closed_accounts_get_different_error_messages(self):
        unactivated_user = UnverifiedUserFactory()
        closed_account = UnactiveUserFactory()

        response1 = self.client.post(
            reverse('auth:token'),
            data={
                'client_id': self.users_app_client_id,
                'client_secret': self.users_app_secret,
                'grant_type': 'password',
                'username': unactivated_user.email,
                'password': FACTORY_PASSWORD,
            }
        ).json()

        response2 = self.client.post(
            reverse('auth:token'),
            data={
                'client_id': self.users_app_client_id,
                'client_secret': self.users_app_secret,
                'grant_type': 'password',
                'username': closed_account.email,
                'password': FACTORY_PASSWORD,
            }
        ).json()

        self.assertNotEqual(response1, response2)

        response3 = self.client.post(
            reverse('auth:token'),
            data={
                'client_id': self.users_app_client_id,
                'client_secret': self.users_app_secret,
                'grant_type': 'password',
                'username': 'Wrong_email@home.com',
                'password': FACTORY_PASSWORD,
            }
        ).json()

        self.assertNotEqual(response1, response3)
        self.assertNotEqual(response2, response3)

    def test_verify_mail(self):
        if not USER_ACTIVATES_ACCOUNT:
            return

        self.client.post(
            reverse('auth:register'),
            data={
                'email': 'tester@home.com',
                'password': FACTORY_PASSWORD
            }
        )
        user = User.objects.last()

        activation_mail = mail.outbox[0]

        pattern = re.compile(r"http:\/\/testdomain.somewhere.com\/auth\/activate\/(?P<uid>[\w_-]*)\/(?P<token>[\w_-]*)")

        match = pattern.search(activation_mail.body)

        self.assertEqual(user.email_verified, False)
        self.assertEqual(user.is_active, False)
        response = self.client.post(
            path=reverse('auth:activate'),
            data=match.groupdict()
        )
        user.refresh_from_db()
        self.assertEqual(response.status_code, 204)
        self.assertEqual(user.is_active, True)
        self.assertEqual(user.email_verified, True)


class AuthorizationCodeFlowTestCases(APITestCase):

    def setUp(self):
        # create `client credentials` type of application to give frontend access to backend
        self.app = Application.objects.create(
            client_id='good_client_id123', client_type='public',
            skip_authorization=True, authorization_grant_type='client-credentials', name='test1'
        )
        # login with application
        response = self.client.post(
            reverse('auth:token'),
            data={
                'client_id': self.app.client_id,
                'client_secret': self.app.client_secret,
                'grant_type': 'client_credentials',  # notice that it's different from self.app.authorization_grant_type
                # so don't change it. It's silly that the difference is an underscore(_) turning into a dash(-) but
                # that's how they designed it. what can we do.
            }
        )
        self.assertEqual(response.status_code, 200, response.content)
        self.token = 'Bearer ' + response.json()['access_token']

        # create `Resource owner password-based` type of application to give frontend access to backend
        self.users_app = Application.objects.create(
            client_id='UsersApp', client_type='public',
            skip_authorization=True, authorization_grant_type='password', name='test1'
        )
        self.user = UserFactory()
        # Fully logged in user
        response = self.client.post(
            reverse('auth:token'),
            data={
                'client_id': self.users_app.client_id,
                'client_secret': self.users_app.client_secret,
                'grant_type': self.users_app.authorization_grant_type,
                'username': self.user.email,
                'password': FACTORY_PASSWORD
            }
        )
        self.user_auth_header = 'Bearer ' + response.json()['access_token']

        self.valid_redirect_uri = 'http://localhost:8000/callback/'
        self.other_product_app = Application.objects.create(
            client_id='OtherSiteApp', client_type='confidential', redirect_uris=self.valid_redirect_uri,
            skip_authorization=True, authorization_grant_type='authorization-code', name='other_product'
        )

    def test_flow(self):
        '''
        Steps: get code, get token, get user information
        Notes: User has to be logged in to get code
        '''
        # getting code without logging in
        response = self.client.get(
            reverse('auth:authorize'),
            data={
                'client_id': self.app.client_id,
                'client_secret': self.app.client_secret,
                'grant_type': 'code',
            }
        )
        self.assertEqual(response.status_code, 401)

        # logging as an app shouldn't work either
        self.client.credentials(HTTP_AUTHORIZATION=self.token)
        # trying to get code again after logging in as an app
        response = self.client.get(
            reverse('auth:authorize'),
            data={
                'client_id': self.app.client_id,
                'client_secret': self.app.client_secret,
                'grant_type': 'code',
            }
        )
        self.assertEqual(response.status_code, 403)

        # Logging a user in
        self.client.credentials(HTTP_AUTHORIZATION=self.user_auth_header)
        # getting code
        response = self.client.get(
            reverse('auth:authorize'),
            data={
                'client_id': self.other_product_app.client_id,
                # 'client_secret': self.other_product_app.client_secret,
                'response_type': 'code',
                'redirect_uri': self.valid_redirect_uri
            }
        )
        self.assertEqual(response.status_code, 200, response.content)
        url, code = response.json()['uri'].split('?')
        code = code.split('=')[1]
        # getting token
        response = self.client.post(
            reverse('auth:token'),
            data={
                'client_id': self.other_product_app.client_id,
                'client_secret': self.other_product_app.client_secret,
                'grant_type': 'authorization_code',
                'code': code,
                'redirect_uri': self.valid_redirect_uri
            }
        )
        self.assertEqual(response.status_code, 200, response.content)
        token = 'Bearer ' + response.json()['access_token']
        # getting user information
        self.client.credentials(HTTP_AUTHORIZATION=token)  # logged in now.
        response = self.client.get(
            reverse('auth:user'),
        )

        self.assertEqual(response.status_code, 200, response.content)
        print(response.content)
