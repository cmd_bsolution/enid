'''
TODO: test user can only apply for his own places
'''
from django.urls import reverse
from django.core import mail

from rest_framework.test import APITestCase

from users.models import User
from quotes.models import QuoteRequest
from ..factories import FACTORY_PASSWORD, PlaceFactory


class QuotesAPITest(APITestCase):

    def setUp(self):
        self.user = User.objects.create_superuser(
            email='admin@home.com',
            password=FACTORY_PASSWORD,
        )

        self.place = PlaceFactory(owner=self.user)

    def test_unauthenticated_users_not_allowed_to_create_quotes(self):
        quote_requests_count = QuoteRequest.objects.count()
        resp = self.client.post(
            reverse('place-quoterequests-list', args=[self.place.pk]),
            data={
                # 'place': "http://localhost:8000" + reverse('place-detail', kwargs={'pk': self.place.pk}),
                "company_name": "First Company",  # name of the recipient vendor
                "company_email": "info@firstcompany.com",  # vendor's email address
                "ps": [
                    {
                        "name": "Product or Service Name",
                        "code": "PS001"
                    },
                    {
                        "name": "Another Product or Service",
                        "code": "PS002"
                    }
                ]
            }
        )
        self.assertEqual(resp.status_code, 401, resp.json())
        self.assertEqual(QuoteRequest.objects.count(), quote_requests_count)

    def test_authenticated_users_can_create_quotes(self):
        quote_requests_count = QuoteRequest.objects.count()
        self.assertTrue(self.client.login(email=self.user.email, password=FACTORY_PASSWORD))

        quote_request_data = {
            "company_name": "First Company",  # name of the recipient vendor
            "company_email": "info@firstcompany.com",  # vendor's email address
            "ps": [
                {
                    "name": "Product or Service Name",
                    "code": "PS001"
                },
                {
                    "name": "Another Product or Service",
                    "code": "PS002"
                }
            ]
        }
        resp = self.client.post(
            reverse('place-quoterequests-list', args=[self.place.pk]),
            data=quote_request_data
        )
        self.assertEqual(resp.status_code, 201, resp.json())
        self.assertEqual(QuoteRequest.objects.count(), quote_requests_count + 1)

        # test mail is out
        self.assertEqual(len(mail.outbox), 1)

        # test another mail is out
        resp = self.client.post(
            reverse('place-quoterequests-list', args=[self.place.pk]),
            data=quote_request_data
        )
        self.assertEqual(resp.status_code, 201, resp.json())
        self.assertEqual(QuoteRequest.objects.count(), quote_requests_count + 2)
        self.assertEqual(len(mail.outbox), 2)

        # test multiple emails are sent when using bulk creation
        resp = self.client.post(
            reverse('place-quoterequests-list', args=[self.place.pk]),
            data=[quote_request_data, quote_request_data, quote_request_data]
        )
        self.assertEqual(resp.status_code, 201, resp.json())
        self.assertEqual(QuoteRequest.objects.count(), quote_requests_count + 5)
        self.assertEqual(len(mail.outbox), 5)

    def test_create_quotes_empty_ps(self):
        self.assertTrue(self.client.login(email=self.user.email, password=FACTORY_PASSWORD))

        quote_request_data = {
            "company_name": "First Company",  # name of the recipient vendor
            "company_email": "info@firstcompany.com",  # vendor's email address
            "ps": []
        }
        resp = self.client.post(
            reverse('place-quoterequests-list', args=[self.place.pk]),
            data=quote_request_data
        )
        self.assertEqual(resp.status_code, 400, resp.json())

    def test_create_quotes_empty_ps_dict(self):
        self.assertTrue(self.client.login(email=self.user.email, password=FACTORY_PASSWORD))

        quote_request_data = {
            "company_name": "First Company",  # name of the recipient vendor
            "company_email": "info@firstcompany.com",  # vendor's email address
            "ps": [{}]
        }
        resp = self.client.post(
            reverse('place-quoterequests-list', args=[self.place.pk]),
            data=quote_request_data
        )
        self.assertEqual(resp.status_code, 400, resp.json())

    def test_create_quotes_invalid_type(self):
        self.assertTrue(self.client.login(email=self.user.email, password=FACTORY_PASSWORD))

        quote_request_data = {
            "company_name": "First Company",  # name of the recipient vendor
            "company_email": "info@firstcompany.com",  # vendor's email address
            "ps": [
                {
                    "name": "Product or Service Name",
                    "code": "PS001"
                },
                {
                    "name": "Another Product or Service",
                    "code": 2
                }
            ]
        }
        resp = self.client.post(
            reverse('place-quoterequests-list', args=[self.place.pk]),
            data=quote_request_data
        )
        self.assertEqual(resp.status_code, 400, resp.json())
