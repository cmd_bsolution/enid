from django.contrib import admin

from utils.admin import TemperateTabularInline
from .models import Feature, Component, Option


class ComponentInline(admin.StackedInline):
    model = Component
    extra = 0
    show_change_link = True


class ParentInline(ComponentInline):
    fk_name = 'parent'


class DependencyInline(ComponentInline):
    fk_name = 'depends_on'


class OptionInline(TemperateTabularInline):
    model = Option
    extra = 5
    show_change_link = True


class OptionAdmin(admin.ModelAdmin):
    inlines = (ParentInline, DependencyInline,)
    list_display = ('__str__', 'score')


class ComponentAdmin(admin.ModelAdmin,):
    inlines = (OptionInline,)
    list_display = ('name', 'parent', 'depends_on')


admin.site.register(Component, ComponentAdmin)
admin.site.register(Option, OptionAdmin)
admin.site.register(Feature)
