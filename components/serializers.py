from rest_framework import serializers

from .models import Component, Option


class OptionSerializer(serializers.HyperlinkedModelSerializer):

    class Meta:
        model = Option
        fields = ('url', 'name', 'component', 'score', 'attributes', 'ps_code')


class ComponentSerializer(serializers.HyperlinkedModelSerializer):
    options = OptionSerializer(many=True, read_only=True)

    class Meta:
        model = Component
        fields = ('url', 'name', 'polar', 'options', 'is_mitigation_measure', 'parent')
