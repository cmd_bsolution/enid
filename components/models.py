from functools import partial

from django.contrib.postgres.fields import JSONField
from django.db import models

from utils.validators import validate_json_schema


class Feature(models.Model):
    name = models.CharField(max_length=50)

    def __str__(self):
        return self.name


weights_keys = [
    "behavior", "delay", "detection", "deterrence", "property_type", "response",
    "crime_demographics", "conditions", "location", "damage", "severity"
]
weights_schema = {
    "type": "object",
    "properties": {key: {"type": "number"} for key in weights_keys},
    "anyOf": [{"required": [key]} for key in weights_keys],
}


class Component(models.Model):
    name = models.CharField(max_length=100)
    polar = models.BooleanField(default=False)
    weights = JSONField(validators=[partial(validate_json_schema, weights_schema)])
    parent = models.ForeignKey(
        'Option',
        models.CASCADE,
        related_name='attributes',
        null=True,
        blank=True,
    )
    depends_on = models.ForeignKey(
        'Option',
        models.CASCADE,
        related_name='subcomponents',
        null=True,
        blank=True,
        help_text=(
            'The component will only be part of the security model if this '
            'option is included.'
        )
    )
    feature = models.ForeignKey(
        Feature,
        models.CASCADE,
        null=True,
        blank=True,
    )
    is_mitigation_measure = models.BooleanField(default=False)

    def __str__(self):
        if self.parent:
            return '%s | %s' % (self.parent, self.name)
        else:
            return self.name


class Option(models.Model):
    name = models.CharField(max_length=100)
    component = models.ForeignKey(
        Component,
        models.CASCADE,
        related_name='options',
    )
    score = models.PositiveSmallIntegerField()
    ps_code = models.SlugField(blank=True)

    def __str__(self):
        return '%s | %s' % (self.component, self.name,)
