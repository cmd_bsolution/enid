FROM python:3.5

ENV PYTHONUNBUFFERED 1

# Should be overriden in other environments
ARG ENVIRONMENT=production

# Requirements have to be pulled and installed here, otherwise caching won't work
COPY ./requirements /requirements

RUN pip install -r /requirements/${ENVIRONMENT}.txt \
    && groupadd -r django \
    && useradd -r -g django django


COPY ./compose/django/entrypoint.sh /entrypoint.sh
COPY ./compose/django/gunicorn.sh /gunicorn.sh
COPY ./compose/django/start-dev.sh /start-dev.sh
RUN sed -i 's/\r//' /entrypoint.sh \
    && sed -i 's/\r//' /gunicorn.sh \
    && sed -i 's/\r//' /start-dev.sh \
    && chmod +x /entrypoint.sh \
    && chown django /entrypoint.sh \
    && chmod +x /start-dev.sh \
    && chown django /start-dev.sh \
    && chmod +x /gunicorn.sh \
    && chown django /gunicorn.sh

COPY . /app
RUN chown -R django /app

WORKDIR /app

ENTRYPOINT ["/entrypoint.sh"]
