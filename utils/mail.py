from django.conf import settings
from django.template import loader
from django.core.mail import EmailMultiAlternatives
from djoser.utils import UserEmailFactoryBase


class MailSomeOneFactory(UserEmailFactoryBase):
    '''
    default:
    from_email: DEFAULT_FROM_EMAIL from settings
    to_email: user passed to from_request or to_mails passed to create method
    '''
    subject_template_name = ''
    plain_body_template_name = ''
    html_body_template_name = ''

    def __init__(self, request=None, user=None, **context):
        '''
        Objects are instantiated with each field assigned from 3 places in this order:
        init arguments, request data, or project settings
        '''
        # set all to None
        self.domain, self.user, self.protocol = None, None, None

        # Update from request
        self.update_from_request(request)

        self.domain = self.domain or (getattr(settings, 'DJOSER', None) and settings.DJOSER['DOMAIN'])
        self.user = user or self.user
        secure_ssl = getattr(settings, 'DJANGO_SECURE_SSL_REDIRECT', False)
        self.protocol = self.protocol or 'https://' if secure_ssl else 'http://'

        self.from_email = getattr(settings, 'DEFAULT_FROM_EMAIL', None)
        self.site_name = 'StorageOS Portal'
        self.context_data = context

    def update_from_request(self, request):
        '''
        If request is passed then we can assume some information from it.
        Like site, domain, request_user, protocol
        '''
        if request:
            self.domain = request.stream.META['HTTP_ORIGIN']
            self.user = request.user
            self.protocol = 'https://' if 'https://' in self.domain else 'http://'
            self.domain = self.domain.lstrip('https://').strip('http://')

    def create(self, to_mails=None, **kwargs):
        context = self.get_context()
        subject = loader.render_to_string(self.subject_template_name, context)
        subject = ''.join(subject.splitlines())

        plain_body = loader.render_to_string(self.plain_body_template_name, context)

        to_mails = to_mails or [self.user.email]
        email_message = EmailMultiAlternatives(
            subject, plain_body, self.from_email,
            to_mails
        )
        if self.html_body_template_name:
            html_body = loader.render_to_string(self.html_body_template_name, context)
            email_message.attach_alternative(html_body, 'text/html')
        return email_message
