from django.contrib import admin


class TemperateTabularInline(admin.TabularInline):
    """
    TabularInline that shows no extra forms in the change view.
    """

    def get_extra(self, request, obj=None, **kwargs):
        """Don't show any extra forms in the change view."""
        if obj:
            return 0
        else:
            return self.extra
