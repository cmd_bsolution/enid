from jsonschema import validate
from jsonschema.exceptions import ValidationError as JSONSchemaValidationError
from django.core.exceptions import ValidationError


# FIXME: Using function-based validator instead of Class-based,
# to be serializable in django migrations (for Component.weights)
def validate_json_schema(schema, value):
    try:
        validate(value, schema)
    except JSONSchemaValidationError as e:
        raise ValidationError(e.message)
