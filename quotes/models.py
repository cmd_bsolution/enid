from django.db import models
from django.contrib.postgres.fields import JSONField
from places.models import Place


class QuoteRequest(models.Model):
    place = models.ForeignKey(Place)
    company_name = models.CharField(max_length=200)
    company_email = models.EmailField()
    ps = JSONField()

    class Meta:
        verbose_name = "Quote Request"
        verbose_name_plural = "Quote Requests"
