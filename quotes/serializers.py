from functools import partial

from rest_framework import serializers
from rest_framework_bulk import BulkSerializerMixin

from places.models import Place
from utils.validators import validate_json_schema

from .models import QuoteRequest


class URLPlaceDefault(object):
    def set_context(self, serializer_field):
        self.place = Place.objects.get(
            pk=serializer_field.context['view'].kwargs['parent_lookup_place'])

    def __call__(self):
        return self.place


class QuoteRequestSerializer(BulkSerializerMixin, serializers.ModelSerializer):
    ps_schema = {
        "type": "array",
        "items": {
            "type" : "object",
            "properties" : {
                "name" : {"type" : "string"},
                "code" : {"type" : "string"},
            },
            "required": ["name", "code"],
        },
        "minItems": 1,
    }

    place = serializers.HyperlinkedRelatedField(
        view_name='place-detail', queryset=Place.objects.all(),
        default=URLPlaceDefault()
    )
    ps = serializers.JSONField(validators=[partial(validate_json_schema, ps_schema)])

    class Meta:
        model = QuoteRequest
        fields = ['place', 'company_name', 'company_email', 'ps']
