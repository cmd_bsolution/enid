from rest_framework.viewsets import GenericViewSet

from rest_framework_extensions.mixins import NestedViewSetMixin
from rest_framework_bulk.mixins import BulkCreateModelMixin

from .serializers import QuoteRequestSerializer
from .models import QuoteRequest


class QuoteViewSet(BulkCreateModelMixin, NestedViewSetMixin, GenericViewSet):
    serializer_class = QuoteRequestSerializer
    queryset = QuoteRequest.objects.all()
