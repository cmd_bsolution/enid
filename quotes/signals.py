from django.db.models.signals import post_save
from django.dispatch import receiver

from utils.mail import MailSomeOneFactory
from .models import QuoteRequest


class QuoteRequestEmailFactory(MailSomeOneFactory):
    subject_template_name = 'quote_request_title.txt'
    plain_body_template_name = 'quote_request.html'
    html_body_template_name = 'quote_request.html'


@receiver(post_save, sender=QuoteRequest)
def quote_request_send_mail_to_vendor(sender, instance, created, **kwargs):
    if not created:
        raise Exception("Quote requests shouldn't be updatable")

    email_context = {
        'quote_request': instance,
    }

    email_factory = QuoteRequestEmailFactory(user=instance.place.owner, **email_context)
    email = email_factory.create(to_mails=[instance.company_email])
    email.send()
