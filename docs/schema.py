from coreapi import Document, Link, Field

BASE_URL = '/api/'
BASE_AUTH_URL = BASE_URL + 'auth/'

SCHEMA = Document(
    title='Zequer Risk Assessment',
    content={
        'auth': {
            'oauth2_code': Link(
                url=BASE_AUTH_URL + 'authorize/',
                action='get',
                description="""
                    Get authorization code.
                """,
                fields=[
                    Field(
                        name="Authorization",
                        required=False,
                        location="header",
                        description="ex. JWT/Bearer XXX"
                    ),
                    Field(
                        name='client_id',
                        required=True,
                        location='query',
                    ),
                    Field(
                        name='redirect_uri',
                        required=True,
                        location='query',
                        description=(
                            "Redirect back to this."
                            "Must be included in the OAuth2 application."
                        )
                    ),
                    Field(
                        name='response_type',
                        required=True,
                        location='query',
                        description="Must be 'code'."
                    ),
                    Field(
                        name='scope',
                        required=False,
                        location='query',
                    ),
                    Field(
                        name='state',
                        required=False,
                        location='query',
                    ),
                ]
            ),
            'new_token': Link(
                url=BASE_AUTH_URL + 'token/',
                action='post',
                description="""
                    Get New/refresh user Token

                    Accepted values for `grant_type`:
                        - Resource owner password-based grants: 'password'
                        - Client Credentials grants: 'client_credentials'
                        - Authorization Code grants: 'authorization_code'
                        - Refreshing access tokens: 'refresh_token'

                    `username` and `password` are only required for Resource owner password-based grants.

                """,
                fields=[
                    Field(
                        name='client_id',
                        required=True,
                        location='formData',
                    ),
                    Field(
                        name='client_secret',
                        required=False,
                        location='formData',
                    ),
                    Field(
                        name='grant_type',
                        required=True,
                        location='formData',
                    ),
                    Field(
                        name='username',
                        required=False,
                        location='formData',
                        description='Required for resource owner password-based grants.'
                    ),
                    Field(
                        name='password',
                        required=False,
                        location='formData',
                        description='Required for resource owner password-based grants.'
                    ),
                    Field(
                        name='refresh_token',
                        required=False,
                        location='formData',
                        description='Required for refreshing a token'
                    ),
                    Field(
                        name='code',
                        required=False,
                        location='formData',
                        description='Required for the authorization code flow'
                    ),
                    Field(
                        name='redirect_uri',
                        required=False,
                        location='formData',
                        description='Required for the authorization code flow'
                    ),
                ]
            ),
            'revoke_token': Link(
                url=BASE_AUTH_URL + 'revoke-token/',
                action='post',
                description="""
                    Revoke an access or refresh token.
                    Can be used as a logout endpoint.
                """,
                fields=[
                    Field(
                        name='client_id',
                        required=True,
                        location='formData',
                    ),
                    Field(
                        name='client_secret',
                        required=False,
                        location='formData',
                    ),
                    Field(
                        name='token',
                        required=True,
                        location='formData',
                    ),
                ],
            ),
            'register': Link(
                url=BASE_AUTH_URL + 'register/',
                action='post',
                description="""
                    Register a new user.
                """,
                fields=[
                    Field(
                        name='Authorization',
                        required=False,
                        location='header',
                        description="Access token prefixed with 'Bearer '"
                    ),
                    Field(
                        name='email',
                        required=True,
                        location='formData',
                    ),
                    Field(
                        name='password',
                        required=True,
                        location='formData',
                    )
                ]
            ),
            'activate': Link(
                url=BASE_AUTH_URL + 'activate/',
                action='post',
                description="""
                    Activate an account.

                    Note: currently accounts are activated automatically.
                """,
                fields=[
                    Field(
                        name="Authorization",
                        required=False,
                        location="header",
                        description="Access token prefixed with 'Bearer '"
                    ),
                    Field(
                        name='uid',
                        required=True,
                        location='formData',
                    ),
                    Field(
                        name='token',
                        required=True,
                        location='formData',
                    ),
                ],
            ),
            'social_login': Link(
                url=BASE_AUTH_URL + 'convert-token/',
                action='post',
                description="""
                    Convert a token from a third-party provider to one that can be used with this API.
                """,
                fields=[
                    Field(
                        name='client_id',
                        required=True,
                        location='formData',
                    ),
                    Field(
                        name='grant_type',
                        required=True,
                        location='formData',
                        description='convert_token'
                    ),
                    Field(
                        name='backend',
                        required=True,
                        location='formData',
                        description="'google-oauth2' or 'facebook'"
                    ),
                    Field(
                        name='token',
                        required=True,
                        location='formData',
                        description='Use Facebook or Google SDK'
                    ),
                ]
            ),
            'me': Link(
                url=BASE_AUTH_URL + 'me/',
                action='get',
                description="""
                    Get user details.
                """,
                fields=[
                    Field(
                        name="Authorization",
                        required=False,
                        location="header",
                        description="Access token prefixed with 'Bearer '"
                    ),
                ]
            ),
            'update_me': Link(
                url=BASE_AUTH_URL + 'me/',
                action='patch',
                description="""
                    Update user details.
                """,
                fields=[
                    Field(
                        name="Authorization",
                        required=False,
                        location="header",
                        description="Access token prefixed with 'Bearer '"
                    ),
                    Field(
                        name='email',
                        required=False,
                        location='formData',
                    ),
                    Field(
                        name='name',
                        required=False,
                        location='formData',
                    ),
                ]
            ),
            'change_password': Link(
                url=BASE_AUTH_URL + 'password/',
                action='post',
                description="""
                    Change password.
                """,
                fields=[
                    Field(
                        name="Authorization",
                        required=False,
                        location="header",
                        description="Access token prefixed with 'Bearer '"
                    ),
                    Field(
                        name='current_password',
                        required=True,
                        location='formData',
                    ),
                    Field(
                        name='new_password',
                        required=True,
                        location='formData',
                    ),
                ],
            ),
            'reset_password': Link(
                url=BASE_AUTH_URL + 'password/reset/',
                action='post',
                description="""
                    Reset password.
                """,
                fields=[
                    Field(
                        name="Authorization",
                        required=False,
                        location="header",
                        description="Access token prefixed with 'Bearer '"
                    ),
                    Field(
                        name='email',
                        required=True,
                        location='formData',
                    ),
                ],
            ),
            'reset_password_confirm': Link(
                url=BASE_AUTH_URL + 'password/reset/confirm/',
                action='post',
                description="""
                    Confirm password reset.
                """,
                fields=[
                    Field(
                        name="Authorization",
                        required=False,
                        location="header",
                        description="Access token prefixed with 'Bearer '"
                    ),
                    Field(
                        name='uid',
                        required=True,
                        location='formData',
                    ),
                    Field(
                        name='token',
                        required=True,
                        location='formData',
                    ),
                    Field(
                        name='new_password',
                        required=True,
                        location='formData',
                    ),
                ],
            ),
            'deactivate_account': Link(
                url=BASE_AUTH_URL + 'deactivate-account/',
                action='post',
                description="""
                    Deactivate account.
                """,
                fields=[
                    Field(
                        name="Authorization",
                        required=False,
                        location="header",
                        description="Access token prefixed with 'Bearer '"
                    )
                ],
            ),
        },
        'quotes': {
            'request': Link(
                url=BASE_URL + 'places/{place_pk}/request-quotes/',
                action='post',
                description="""
                    Request a quote.

                    In order to create more than one QuoteRequest, use this format
                    [{..}, {..}, {..}] where {..} is each QuoteRequest.

                    all quotes created together have to belong to the same place, whose id is in the URL.
                """,
                fields=[
                    Field(
                        name="place_pk",
                        required=True,
                        location="path",
                    ),
                    Field(
                        name="company_name",
                        required=True,
                        location="form",
                    ),
                    Field(
                        name="company_email",
                        required=True,
                        location="form",
                    ),
                    Field(
                        name="ps",
                        required=False,
                        location="form",
                    ),
                ]
            ),
        }
    }
)
