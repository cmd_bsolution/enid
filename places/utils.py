from statistics import mean


class SecurityRatingCalculator(object):
    def __init__(self, options):
        self.options = options

    def get_options_mean(self, factor, parent_option=None):
        options = self.options.filter(
            component__weights__has_key=factor,
            component__parent=parent_option
        )

        scores = []
        weights = []
        if options:
            for option in options:
                scores.append(self.get_options_mean(factor, option))
                weights.append(option.component.weights[factor])

        if parent_option:
            scores.append(parent_option.score)
            weights.append(sum(weights) if weights else 1)

        return (
            sum(score * weight for score, weight in zip(scores, weights))
            / sum(weights)
        )

    def calculate(self):
        vulnerability_factors = (
            'behavior',
            'delay',
            'detection',
            'deterrence',
            'property_type',
            'response',
        )
        threat_factors = (
            'crime_demographics',
            'conditions',
            'location',
        )

        vulnerability = mean(map(self.get_options_mean, vulnerability_factors))
        threat = mean(map(self.get_options_mean, threat_factors))
        damage = self.get_options_mean('damage')
        severity = self.get_options_mean('severity')

        return vulnerability * threat * (damage + severity)
