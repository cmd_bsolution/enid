from itertools import chain
from django.db import transaction

from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework.decorators import list_route

from users.permissions import IsOwner
from components.models import Option

from .models import Place
from .serializers import PlaceSerializer, SecurityRatingSerializer
from .utils import SecurityRatingCalculator


class PlaceViewSet(viewsets.ModelViewSet):
    queryset = Place.objects.all()
    serializer_class = PlaceSerializer
    permission_classes = [IsOwner]

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)

    # Guarantee atomicity on partial updates.
    @transaction.atomic
    def partial_update(self, request, *args, **kwargs):
        return super().partial_update(request, *args, **kwargs)

    @list_route(methods=['POST'])
    def calculate_security_rating(self, request, format=None):
        serializer = SecurityRatingSerializer(data=request.data)
        # import ipdb; ipdb.set_trace()
        serializer.is_valid(raise_exception=True)
        options = serializer.validated_data['options']

        calculator = SecurityRatingCalculator(
            Option.objects.filter(id__in=[o.id for o in options]))
        response = {
            'security_rating': calculator.calculate(),
        }
        return Response(response)
