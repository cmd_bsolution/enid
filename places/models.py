from statistics import mean

from django.conf import settings
from django.db import models

from components.models import Option

from .utils import SecurityRatingCalculator


class Place(models.Model):
    address = models.CharField(max_length=250)
    name = models.CharField(max_length=50)
    options = models.ManyToManyField(Option)
    owner = models.ForeignKey(settings.AUTH_USER_MODEL, models.CASCADE)

    @property
    def security_rating(self):
        return SecurityRatingCalculator(self.options.all()).calculate()

    def __str__(self):
        return self.address
