from django.contrib import admin

from .models import Place


class PlaceAdmin(admin.ModelAdmin):
    readonly_fields = ('security_rating',)


admin.site.register(Place, PlaceAdmin)
