from __future__ import absolute_import
from itertools import chain

from rest_framework import serializers

from components.models import Option

from .models import Place

FACTORS = set([
    'behavior',
    'delay',
    'detection',
    'deterrence',
    'property_type',
    'response',
    'crime_demographics',
    'conditions',
    'location',
    'severity',
    'damage',
])


class OptionValidationMixin(object):
    def validate_options(self, options):
        weights = set(chain.from_iterable(option.component.weights.keys() for option in options))
        missed_weights = FACTORS - weights
        if missed_weights:
            raise serializers.ValidationError(
                'Those factors are missed in your data: {}'.format(", ".join(missed_weights))
            )
        return options


class PlaceSerializer(OptionValidationMixin, serializers.HyperlinkedModelSerializer):

    class Meta:
        model = Place
        fields = ('url', 'address', 'name', 'options', 'security_rating',)


class SecurityRatingSerializer(OptionValidationMixin, serializers.Serializer):
    options = serializers.HyperlinkedRelatedField(
        view_name='option-detail', queryset=Option.objects.all(), many=True
    )
