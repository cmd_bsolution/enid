from django.contrib.auth.admin import UserAdmin as DefaultUserAdmin
from django.contrib import admin
from .models import User


class UserAdmin(DefaultUserAdmin):
    fieldsets = (
        (None, {
            'fields': (
                'email',
                'password',
            )
        }),
        ('Permissions', {
            'fields': (
                'is_active',
                'is_staff',
                'is_superuser',
            )
        }),
    )
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': (
                'email',
                'password1',
                'password2',
            ),
        }),
    )
    list_display = ('email', 'is_staff',)
    list_filter = ('is_staff', 'is_superuser', 'is_active',)
    search_fields = ('email',)
    ordering = ('email',)
    filter_horizontal = ()


admin.site.register(User, UserAdmin)
