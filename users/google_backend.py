"""
This file overrides social_core.backends.google.GoogleOAuth2 which is
problematic when used with django_rest_social_auth.
"""

from social_core.backends.google import GoogleOAuth2 as DefaultGoogleOAuth2
from django.conf import settings


class GoogleOAuth2(DefaultGoogleOAuth2):
    def user_data(self, access_token, *args, **kwargs):
        """Return user data from Google API"""
        return self.get_json(
            'https://www.googleapis.com/oauth2/v3/tokeninfo',
            params={
                'id_token': access_token,
                'alt': 'json',
                'key': settings.SOCIAL_AUTH_GOOGLE_OAUTH2_PROJECT_KEY,
            }
        )
