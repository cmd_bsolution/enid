import json

from django.conf import settings
from django.views.generic import TemplateView

from djoser import views as djoser_views
from oauth2_provider.exceptions import OAuthToolkitError
from oauth2_provider.contrib.rest_framework import TokenHasReadWriteScope
from oauth2_provider.oauth2_backends import OAuthLibCore
from oauth2_provider.settings import oauth2_settings
from oauth2_provider.views.mixins import OAuthLibMixin
from rest_framework import status, permissions
from rest_framework.views import APIView
from rest_framework.response import Response

from .serializers import LoginSerializer


class AuthorizationView(OAuthLibMixin, APIView):

    server_class = oauth2_settings.OAUTH2_SERVER_CLASS
    validator_class = oauth2_settings.OAUTH2_VALIDATOR_CLASS
    oauthlib_backend_class = oauth2_settings.OAUTH2_BACKEND_CLASS

    def get(self, request, *args, **kwargs):
        try:
            scopes, credentials = self.validate_authorization_request(request)
            uri, headers, body, response_status = self.create_authorization_response(
                request=self.request, scopes=" ".join(scopes),
                credentials=credentials, allow=True)
            if response_status == 302:
                response_status = status.HTTP_200_OK
            return Response(data={'uri': uri}, status=response_status)

        except OAuthToolkitError as error:
            return self.error_response(error)

    def error_response(self, error, **kwargs):
        """
        Handle errors either by redirecting to redirect_uri with a json
        in the body containing error details or providing an error
        response.
        """
        redirect, error_response = super().error_response(error, **kwargs)

        status = error_response['error'].status_code
        return Response(str(error_response), status=status)


class TokenView(OAuthLibMixin, APIView):
    """
    Implements an endpoint to provide access tokens

    The endpoint is used in the following flows:

    * Authorization code
    * Password
    * Client credentials
    """
    server_class = oauth2_settings.OAUTH2_SERVER_CLASS
    validator_class = oauth2_settings.OAUTH2_VALIDATOR_CLASS
    oauthlib_backend_class = OAuthLibCore
    permission_classes = (permissions.AllowAny,)

    def post(self, request, *args, **kwargs):
        """
        Use REST framework's `.data` to fake the post body of the django
        request.
        """
        request._request.POST = request._request.POST.copy()
        if 'username' in request.data.keys():
            login_serializer = LoginSerializer(data=request.data)
            login_serializer.is_valid(raise_exception=True)
        for key, value in request.data.items():
            request._request.POST[key] = value
        url, headers, body, status = self.create_token_response(request._request)
        response = Response(data=json.loads(body), status=status)

        for k, v in headers.items():
            response[k] = v
        return response


class DeactivateAccountView(APIView):

    permission_classes = (
        permissions.AllowAny, TokenHasReadWriteScope
    )

    def post(self, request, *args, **kwargs):
        """
        Sets is_active to False and removes all associated tokens
        """
        self.request.user.is_active = False
        self.request.user.save()
        # Revoke all tokens
        self.request.user.oauth2_provider_accesstoken.all().delete()
        return Response({'message': 'success'}, status=status.HTTP_200_OK)


class ActivationView(djoser_views.ActivationView):
    """
    Use this endpoint to activate user email address.
    """

    permission_classes = (
        permissions.AllowAny
    )

    def _action(self, serializer):
        """
        On activation, both email_verified, and is_active should be set to True.
        """
        serializer.user.email_verified = True
        return super()._action(serializer)


class LoginView(djoser_views.LoginView):

    permission_classes = (
        permissions.AllowAny, TokenHasReadWriteScope
    )


class LogoutView(djoser_views.LogoutView):

    permission_classes = (
        permissions.IsAuthenticated, TokenHasReadWriteScope
    )


class PasswordResetView(djoser_views.PasswordResetView):

    permission_classes = (
        permissions.AllowAny, TokenHasReadWriteScope
    )


class SetPasswordView(djoser_views.SetPasswordView):

    permission_classes = (
        permissions.IsAuthenticated, TokenHasReadWriteScope
    )


class PasswordResetConfirmView(djoser_views.PasswordResetConfirmView):

    permission_classes = (
        permissions.AllowAny, TokenHasReadWriteScope
    )


class UserView(djoser_views.UserView):

    permission_classes = (
        permissions.IsAuthenticated, TokenHasReadWriteScope
    )


class SocialCallBackView(TemplateView):

    template_name = 'callback.html'

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context.update(
            {'social_auth_google_client_id': settings.SOCIAL_AUTH_GOOGLE_OAUTH2_KEY}
        )
        return context

    def get(self, request, *args, **kwargs):
        context = self.get_context_data(**kwargs)
        context.update({'data': self.request.GET.dict()})
        return self.render_to_response(context)

    def post(self, request, *args, **kwargs):
        context = self.get_context_data(**kwargs)
        context.update({'data': self.request.POST.dict()})
        return self.render_to_response(context)
