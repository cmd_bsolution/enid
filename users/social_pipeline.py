from django.contrib.auth import get_user_model
from rest_framework.response import Response


def check_for_user(backend, uid, user=None, *args, **kwargs):
    if ('user' in kwargs and not kwargs['user']) or 'user' not in kwargs:
        User = get_user_model()
        user = User.objects.filter(email=kwargs['details']['email'])
        if user:
            return {'user': user.get()}


def check_for_email(backend, uid, user=None, *args, **kwargs):
    if not kwargs['details'].get('email'):
        return Response({'error': "Email wasn't provided by facebook"}, status=400)


def save_name(strategy, details, user=None, *args, **kwargs):
    """Update user name using data from provider."""
    if user:
        changed = False  # flag to track changes
        if 'fullname' in details:
            changed |= details['fullname'] != user.name  # 'changed = changed or right_hand_side'
            user.name = details['fullname']
        if changed:
            strategy.storage.user.changed(user)
