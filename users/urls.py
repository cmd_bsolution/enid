from django.conf.urls import include, url

from djoser import views as djoser_views

from . import views

urlpatterns = [
    url(r'^me/$', views.UserView.as_view(), name='user'),
    url(r'^register/$', djoser_views.RegistrationView.as_view(), name='register'),
    url(r'^password/$', views.SetPasswordView.as_view(), name='set_password'),
    url(r'^password/reset/$', views.PasswordResetView.as_view(), name='password_reset'),
    url(r'^password/reset/confirm/$', views.PasswordResetConfirmView.as_view(), name='password_reset_confirm'),
    url(r'^token/$', views.TokenView.as_view(), name="token"),
    url(r'^deactivate-account/$', views.DeactivateAccountView.as_view(), name='deactivate_account'),
    url(r'^activate/$', views.ActivationView.as_view(), name='activate'),
    url(r'^authorize/$', views.AuthorizationView.as_view(), name='authorize'),
    url(r'^social/', include('rest_framework_social_oauth2.urls', namespace='drfso2')),
]
