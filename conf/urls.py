from django.conf import settings
from django.conf.urls import url, include

from rest_framework_extensions.routers import ExtendedDefaultRouter

from users.views import SocialCallBackView
from components import views as components_views
from places import views as places_views
from questions import views as questions_views
from quotes import views as quotes_views
from docs.views import SchemaView
from .admin import admin

router = ExtendedDefaultRouter()
router.register(r'components', components_views.ComponentViewSet)
router.register(r'options', components_views.OptionViewSet)
router.register(r'questions', questions_views.QuestionViewSet)
router.register(r'choices', questions_views.ChoiceViewSet)
places = router.register(r'places', places_views.PlaceViewSet)
places.register(
    r'request-quotes', quotes_views.QuoteViewSet,
    base_name='place-quoterequests',
    parents_query_lookups=['place']
)

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^api/', include(router.urls)),
    url(r'^api/auth/', include('users.urls', namespace='auth')),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    url('', include('social_django.urls', namespace='social')),
    url(r'^docs/$', SchemaView.as_view(), name='docs')
]

if settings.DEBUG:
    urlpatterns.append(url(r'^callback/', SocialCallBackView.as_view()))
