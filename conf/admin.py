from django.contrib import admin
from django.contrib.auth.models import Group

admin.site.site_header = 'Administration Panel'
admin.site.site_title = 'Administration Panel'
admin.site.site_url = None
admin.site.index_title = 'Index'

admin.site.unregister(Group)
