from .base import *

DEBUG = True

INSTALLED_APPS = INSTALLED_APPS + ['django_extensions']

CORS_ORIGIN_ALLOW_ALL = True

REST_FRAMEWORK['DEFAULT_RENDERER_CLASSES'] = (
    'rest_framework.renderers.JSONRenderer',
    'rest_framework.renderers.BrowsableAPIRenderer',
)

SWAGGER_SETTINGS.update({
    'LOGIN_URL': '/api-auth/login/',
    'LOGOUT_URL': '/api-auth/logout/',
})


# Mail settings for console viewing
SERVER_EMAIL = DEFAULT_FROM_EMAIL
